/* jshint browser:true */
document.addEventListener('DOMContentLoaded', function() 
{
    var swiffyView = document.getElementById('swiffycontainer');
    var remainingWindowHeight = window.innerHeight - 240.0;
    var swiffyRatioByWidth = window.innerWidth / 176.0;        
    
    if (swiffyRatioByWidth * 208.0 < remainingWindowHeight)
    {
        swiffyView.style.zoom = swiffyRatioByWidth;
    }
    else
    {
        var swiffyRatioHeight = remainingWindowHeight / 208.0;
        swiffyView.style.zoom = swiffyRatioHeight;
    }
});

function fireKeyEvent(code)
{
    var keyboardEvent = document.createEvent("KeyboardEvent");
    var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";

    keyboardEvent[initMethod](
                       "keydown", // event type : keydown, keyup, keypress
                        true, // bubbles
                        false, // cancelable
                        window, // viewArg: should be window
                        false, // ctrlKeyArg
                        false, // altKeyArg
                        false, // shiftKeyArg
                        false, // metaKeyArg
                        code, // keyCodeArg : unsigned long the virtual key code, else 0
                        0 // charCodeArgs : unsigned long the Unicode character associated with the depressed key, else 0
    );
    keyboardEvent.keyCodeVal = code;
    document.body.dispatchEvent(keyboardEvent);    
}

function fireAsciiEvent(code)
{
    var keyboardEvent = document.createEvent("KeyboardEvent");
    var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";

    keyboardEvent[initMethod](
                       "keydown", // event type : keydown, keyup, keypress
                        true, // bubbles
                        false, // cancelable
                        window, // viewArg: should be window
                        false, // ctrlKeyArg
                        false, // altKeyArg
                        false, // shiftKeyArg
                        false, // metaKeyArg
                        0, // keyCodeArg : unsigned long the virtual key code, else 0
                        code // charCodeArgs : unsigned long the Unicode character associated with the depressed key, else 0
    );
    keyboardEvent.charCodeVal = code;
    document.body.dispatchEvent(keyboardEvent);    
}